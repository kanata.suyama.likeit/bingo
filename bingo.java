import java.util.ArrayList;
import java.util.Collections;

public class bingo {
	public static void main(String[] args) {

		// B列の準備
		ArrayList<Integer> bColumn = getColumnNumber(1, 15);
		// I列の準備
		ArrayList<Integer> iColumn = getColumnNumber(16, 30);
		// N列の準備
		ArrayList<Integer> nColumn = getColumnNumber(31, 45);
		// G列の準備
		ArrayList<Integer> gColumn = getColumnNumber(46, 60);
		// O列の準備
		ArrayList<Integer> oColumn = getColumnNumber(61, 75);

		// ビンゴカード
		String[][] bingoCard = {
				{ "", "", "", "", "" },
				{ "", "", "", "", "" },
				{ "", "", "FREE", "", "" },
				{ "", "", "", "", "" },
				{ "", "", "", "", "" }
		};
		//抽選番号
		ArrayList<Integer> num1 = new ArrayList<Integer>();
		for (int i = 1; i <= 75; i++) {
			num1.add(i);
		}
		Collections.shuffle(num1);
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (j == 0) {
					if (bColumn.get(i) < 10) {
						bingoCard[i][j] = "0" + bColumn.get(i).toString();
					} else {
						bingoCard[i][j] = bColumn.get(i).toString();
					}
				}
				if (j == 1) {
					bingoCard[i][j] = iColumn.get(i).toString();
				}
				if (j == 2) {
					if (i == 2 && j == 2) {
						continue;
					}
					bingoCard[i][j] = nColumn.get(i).toString();
				}
				if (j == 3) {
					bingoCard[i][j] = gColumn.get(i).toString();
				}
				if (j == 4) {
					bingoCard[i][j] = oColumn.get(i).toString();
				}
			}
		}
		int ball = 0;

		for (Integer num : num1) {
			int bingonum = 0;
			int rearchNum = 0;
			ball++;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {

					String numString;
					if (num < 10) {
						numString = "0" + num.toString();
					} else {
						numString = num.toString();
					}

					if (bingoCard[i][j].equals(numString)) {
						bingoCard[i][j] = "(" + bingoCard[i][j] + ")";
					}

				}
			}

			// 左上から右下斜めにリーチがあるか
			int holeCount = 0;
			for (int i = 0; i < bingoCard.length; i++) {
				if (bingoCard[i][i].contains("(")) {
					holeCount++;
				}
			}
			if (holeCount == 3) {
				rearchNum++;
			} else if (holeCount == 4) {
				bingonum++;
			}
			// 右上から左下斜めにリーチがあるか
			int holeCount1 = 0;
			int right = 4;
			for (int i = 0; i < bingoCard.length; i++) {
				if (bingoCard[i][right].contains("(")) {
					holeCount1++;
				}
				right--;
			}
			if (holeCount1 == 3) {
				rearchNum++;
			} else if (holeCount1 == 4) {
				bingonum++;
			}
			//横の選別
			for (int a = 0; a < 5; a++) {
				int holeCount2 = 0;
				for (int j = 0; j < 5; j++) {
					if (bingoCard[a][j].contains("(")) {
						holeCount2++;
					}
				}
				if (a == 2 && holeCount2 == 4) {
					bingonum++;
				} else if (a == 2 && holeCount2 == 3) {
					rearchNum++;
				} else if (holeCount2 == 5) {
					bingonum++;
				} else if (holeCount2 == 4) {
					rearchNum++;
				}
			}
			//縦の選別
			int diagonaleRarchNum4 = 0;
			int diagonalBingoNum4 = 0;
			for (int a = 0; a < 5; a++) {
				int holeCount3 = 0;
				for (int j = 0; j < 5; j++) {
					if (bingoCard[j][a].contains("(")) {
						holeCount3++;
					}
				}
				if (a == 2 && holeCount3 == 4) {
					bingonum++;
				} else if (a == 2 && holeCount3 == 3) {
					rearchNum++;
				} else if (holeCount3 == 5) {
					bingonum++;
				} else if (holeCount3 == 4) {
					rearchNum++;
				}
			}

			System.out.println(ball + "回目" + "" + "抽選番号" + num + "番");
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if (bingoCard[i][j].contains("(")) {
						System.out.print(bingoCard[i][j]);
					} else if (bingoCard[i][j].equals("FREE")) {
						System.out.print(bingoCard[i][j]);
					} else {
						System.out.print(" " + bingoCard[i][j] + " ");
					}
				}
				System.out.println();
			}

			System.out.println();
			System.out.println("リーチ数" + rearchNum + "です");
			System.out.println("ビンゴ数" + bingonum + "です");
			System.out.println("--------------------");
			if (bingonum == 12) {
				break;
			}
		}
	}

	// B~O列の数字を格納するメソッド
	public static ArrayList<Integer> getColumnNumber(int startNum, int endNum) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = startNum; i <= endNum; i++) {
			list.add(i);
		}
		Collections.shuffle(list);

		return list;
	}

}
